package mapping.amazone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 18.02.2017.
 */
public class SearchResultsPage {

    private WebDriver driver;

    public SearchResultsPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//h2[contains(@class, 's-access-title')]")
    public List<WebElement> searchResults;

    @FindBy (css = ".cell2 .acs-mn2-cellTitle")
    public WebElement babyProductsCategoryButton;

    @FindBy (id = "s-result-count")
    public WebElement numberOfSearchResults;

    @FindBy (xpath = ".//h2[contains(@class, 's-access-title') and contains(text(), '8 Inch')]")
    public WebElement eightInchKnife;

    public Integer getNumberOfSearchResults() {
        String textWithCounter = numberOfSearchResults.getText();
        String[] arrayWithCounter = textWithCounter.split(" ");
        return Integer.parseInt(arrayWithCounter[2].replace(",",""));
    }
}
