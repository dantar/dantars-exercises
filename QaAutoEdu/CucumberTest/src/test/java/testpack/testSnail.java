package testpack;

import games.*;
import org.junit.*;

public class TestSnail {

    Snail snail = new Snail();

    @Test
    // Проверка наибольшего числа в матрице
    public void testMaxNumber() {
        Integer[][] matrix = snail.calculateSnail(5);
        int Max = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (matrix[i][j] > Max) {
                    Max = matrix[i][j];
                }
            }
        }

        Assert.assertEquals("Максимальный элемент матрицы " + Max + ", ожидаемый максимальный элемент: " + 25, 25, Max);
    }

    @Test
    // Проверка расположения максимального элемента при нечётном размере матрицы
    public void testPositionMaxOdd() {
        Integer[][] matrix = snail.calculateSnail(5);
        int max = 0;
        int [] maxPosition = {0,0};
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5 ; j++) {
                if (matrix[i][j] > max) {
                    max = matrix[i][j];
                    maxPosition[0] = i;
                    maxPosition[1] = j;
                }
            }
        }
        int[] expMaxPosition = {0,4};
        Assert.assertArrayEquals("Координаты максимального элемента " + maxPosition[0] + " " + maxPosition[1] + " , ожидаемые были 0 4", expMaxPosition,maxPosition);
    }

    @Test
    // Проверка расположения максимального элемента при чётном размере матрицы
    public void checkPositionOfMaxElementEven() {
        Integer[][] matrix = snail.calculateSnail(4);
        int max = 0;
        int [] maxPosition = {0,0};
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (matrix[i][j] > max) {
                    max = matrix[i][j];
                    maxPosition[0] = i;
                    maxPosition[1] = j;
                }
            }
        }
        int[] expMaxPosition = {3,0};
        Assert.assertArrayEquals("Координаты максимального элемента " + maxPosition[0] + " " + maxPosition[1] + " , ожидаемые были 3 0", expMaxPosition,maxPosition);
    }

    @Test
    // Проверка создания матрицы (возвращает ли метод матрицу)
    public void checkReturnMatrix() {
        Integer[][] matrix = snail.calculateSnail(3);
        Assert.assertNotNull("Метод не вернул матрицу",matrix);
    }

}
