package mapping.stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 19.02.2017.
 */
public class StartPage {

    private WebDriver driver;

    public StartPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(className = "bounty-indicator-tab")
    public WebElement quantityOfFeatured;

    @FindBy(xpath = ".//header//a[@class=\"login-link btn\"]")
    public WebElement signInButton;

    @FindBy(xpath = ".//*[@class='question-hyperlink']")
    public List<WebElement> questionLinksList;

    @FindBy(xpath = ".//*[@class='opt salary']")
    public List<WebElement> salaryesList;

    @FindBy(xpath = ".//header//a[@class=\"login-link btn-clear\"]")
    public WebElement logInButton;

    @FindBy(className = "-avatar")
    public WebElement avatar;


}
