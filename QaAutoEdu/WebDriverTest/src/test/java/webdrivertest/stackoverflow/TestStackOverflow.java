package webdrivertest.stackoverflow;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dantar on 12.02.2017.
 */
public class TestStackOverflow {

    WebDriver driver;

    @Before
    public void setupStartConditions() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://stackoverflow.com/");
    }

    @After
    public void quitDriver() {
        driver.quit();
    }

    @Test
    public void checkQuantityOfFeatured() {
        int quantityOfFeatured = Integer.parseInt(driver.findElement(By.className("bounty-indicator-tab")).getText());
        Assert.assertTrue("Quantity of featured is less than 300",quantityOfFeatured>300);
    }

    @Test
    public void checkSocialAutorisationButtonsIsDisplayed() {
        driver.findElement(By.xpath(".//header//a[@class=\"login-link btn\"]")).click();
        Assert.assertTrue("Page don't have Google autorizatin button", driver.findElement(By.className("google-login")).isDisplayed());
        Assert.assertTrue("Page don't have Facebook autorizatin button", driver.findElement(By.className("facebook-login")).isDisplayed());
    }

    @Test
    public void checkTopQuestionIsToday(){
        driver.findElement(By.xpath(".//*[@class='question-hyperlink']")).click();
        String testDate = driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]/td[2]")).getText();
        Assert.assertTrue("This question was asked not today",testDate.contains("today"));
    }

    @Test
    public void checkAwesomeJob () {
        List<WebElement> salaryList = driver.findElements(By.xpath(".//*[@class='opt salary']"));
        Double higherSalary = 0.0;
        if (salaryList.size() == 0) Assert.assertTrue("There is no jobs with salaries",false);
        else {
            for (WebElement salary: salaryList) {
                String[] salaryArray = salary.toString().replaceAll(",","").split(" - ");

                Double salaryAmount = Double.valueOf(salaryArray[1].substring(1));
                System.out.println(salaryAmount);
                if (salaryAmount>higherSalary) higherSalary=salaryAmount;
            }

            Assert.assertTrue("All salaries are less than 60k",higherSalary>60000);
        }
    }

    @Test
    public void checkLogIn() {
        /*Проверяем залогин в аккаунт по появлению логина в шапке
            1. Кликаем на кнопку логина в шапке
            2. Вводим адрес и пароль
            3. Нажимаем кнопку залогина
            4. Проверяем, что залогинились по наличию аватара
         */
        driver.findElement(By.xpath(".//header//a[@class=\"login-link btn-clear\"]")).click();
        driver.findElement(By.id("email")).sendKeys("lambru100@yandex.ru");
        driver.findElement(By.id("password")).sendKeys("gfhjkm13");
        driver.findElement(By.id("submit-button")).click();
        Assert.assertTrue("We are not logged in - there is no avatar on page",driver.findElement(By.className("-avatar")).isDisplayed());






    }






}
