@oop
Feature: oop

  @Non-static
  Scenario: 001 Circle is instance of Shape
    Given I create Circle with coordinates "0","0" and size "10"
    Then Circle is instance of Shape

  @Non-static
  Scenario: 002 The area of a circle equals size^2*Pi
    Given I create Circle with coordinates "0","0" and size "10"
    Then Area of Circle equals 314

  @Non-static
  Scenario: 003 Moving the Circle changes its coordinates by certain step
    Given I create Circle with coordinates "0","0" and size "10"
    When I move Circle by 8,3
    Then Coordinates of Circle equals 8,3

  @Non-static
  Scenario: 004 Resizing the Circle changes its size by coefficient
    Given I create Circle with coordinates "0","0" and size "10"
    When I resize circle by coefficient 0.9
    Then Size of Circle equals 9

  @Non-static
  Scenario: 005 Square is instance of Shape
    Given I create Square with coordinates "0", "0" and size "10"
    Then Square is instance of Shape

  @Non-static
  Scenario: 006 The area of a square equals size^2
    Given I create Square with coordinates "0", "0" and size "10"
    Then Area of Square equals 100

  @Non-static
  Scenario: 007 Moving the Square changes its coordinates by certain step
    Given I create Square with coordinates "0", "0" and size "10"
    When I move Square by 8,3
    Then Coordinates of Square equals 8,3

  @Non-static
  Scenario: 008 Resizing the Square changes its size by coefficient
    Given I create Square with coordinates "0", "0" and size "10"
    When I resize square by coefficient 0.9
    Then Size of Square equals 9