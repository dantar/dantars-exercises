package testpack;

import org.junit.*;
import oop.*;

public class TestCircle {
    public static Circle circle;

    @Before
    public void createCircle() {
        circle = new Circle(0, 0, 10);
    }

    @After
    public void cleanUp() {
        circle = null;
    }

    @Test
    // Проверка наследования
    public void checkInstance() {
        Assert.assertTrue(circle instanceof Shape);
    }

    @Test
    // Проверка вычисления площади
    public void checkCircleArea() {
        double circleArea = circle.area();
        Assert.assertEquals("Ожидаемый результат был 314.16, но текущий результат " + circleArea, 314.16, circleArea, 0.2);
    }

    @Test
    // Проверка перемещения
    public void checkCircleMove() {
        circle.move(3,7);
        double[] realCoord = {circle.getX(), circle.getY()};
        double[] expCoord = {3,7};
        Assert.assertArrayEquals("Координаты не совпадают",expCoord,realCoord,0.1);
    }

    @Test
    // Проверка изменения размера
    public void checkCircleResize() {
        circle.resize(0.5);
        double resizedRadius = circle.getSize();
        Assert.assertEquals("Неверный размер " + resizedRadius, 5.0, resizedRadius, 0.1);
    }

}
