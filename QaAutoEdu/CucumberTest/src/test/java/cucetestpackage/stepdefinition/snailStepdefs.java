package cucetestpackage.stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import games.Snail;

/**
 * Created by garkusha on 08.02.2017.
 */
public class snailStepdefs {

    private Snail snail;
    private int[][] matrix;
    private int[] maxPosition = {0, 0};

    @Given("^I start program Snail$")
    public void iStartProgramSnail() throws Throwable {
        snail = new Snail();
    }

    @When("^I create snail with size (\\d+)$")
    public void iCreateSnailWithSize(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^position of maximum element is (\\d+),(\\d+)$")
    public void positionOfMaximumElementIs(int arg0, int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^program returns Matrix with size (\\d+)$")
    public void programReturnsMatrixWithSize(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^maximum element is (\\d+)$")
    public void maximumElementIs(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
