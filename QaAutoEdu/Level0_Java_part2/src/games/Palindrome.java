package games;

public class Palindrome {
    public boolean checkWord (String someWord){
        Integer n;
        Integer length = someWord.length();
        if (length % 2 == 0) n = length/2;
        else n = (length-1)/2;

        for (int i = 0; i <= n; i++){
            String first = Character.toString(someWord.charAt(i));
            String last = Character.toString(someWord.charAt(length-1-i));
            if (first.compareToIgnoreCase(last) != 0) {
                return false;
            }
        }

        return true;
    }

    public boolean checkPhrase (String somePhrase){
        somePhrase = somePhrase.replaceAll(" ","");
        return checkWord(somePhrase);
    }



}
