package mapping.amazone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Dantar on 18.02.2017.
 */
public class MerchPage {

    private WebDriver driver;

    public MerchPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (id = "priceblock_ourprice")
    public WebElement merchPrice;

    @FindBy (id = "productTitle")
    public WebElement merchName;

    @FindBy (id = "add-to-cart-button")
    public WebElement addToCartButton;

    @FindBy (id = "nav-cart")
    public WebElement goToCartButton;

    public Double getMerchPrice() {
        return Double.valueOf(merchPrice.getText().replace("$",""));
    }


}
