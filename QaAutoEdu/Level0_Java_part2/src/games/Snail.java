package games;

public class Snail {
    public Integer [][] calculateSnail(Integer size) {
        Integer[][] snailmatrix = new Integer[size][size];
        Integer direction;  //1 - right, 2 - up, 3 - left, 4 - down;
        Integer rowStart = 0;
        Integer rowEnd = size-1;
        Integer colStart = 0;
        Integer colEnd = size-1;
        Integer currentNumber = size * size;

        //Выбор начального направления
        if (size % 2 == 0) direction = 1;
        else direction = 3;

        //Наполнение матрицы
        while (currentNumber > 0) switch (direction) {
            //right
            case 1: {
                for (int i = rowStart; i <= rowEnd; i++) {
                    snailmatrix[colEnd][i] = currentNumber;
                    currentNumber--;
                }
                colEnd--;
                direction = 2;
                break;
            }

            //up
            case 2: {
                for (int i = colEnd; i >= colStart; i--) {
                    snailmatrix[i][rowEnd] = currentNumber;
                    currentNumber--;
                }
                rowEnd--;
                direction = 3;
                break;
            }

            //left
            case 3: {
                for (int i = rowEnd; i >= rowStart; i--) {
                    snailmatrix[colStart][i] = currentNumber;
                    currentNumber--;
                }
                colStart++;
                direction = 4;
                break;
            }

            //down
            case 4: {
                for (int i = colStart; i <= colEnd; i++) {
                    snailmatrix[i][rowStart] = currentNumber;
                    currentNumber--;
                }
                rowStart++;
                direction = 1;
                break;
            }

            default: {
                System.out.println("Что-то пошло не так");
                break;
            }
        }

        return snailmatrix;

    }

}

