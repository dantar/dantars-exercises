package tests.stackoverflow;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import mapping.stackoverflow.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dantar on 12.02.2017.
 */
public class TestStackOverflow {

    public static StartPage startPage;
    public static LogInPage logInPage;
    public static QuestionPage questionPage;

    public static WebDriver driver;

    @Before
    public void setupStartConditions() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        startPage = new StartPage(driver);
        logInPage = new LogInPage(driver);
        questionPage = new QuestionPage(driver);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://stackoverflow.com/");
    }

    @After
    public void quitDriver() {
        driver.quit();
    }

    @Test
    public void checkQuantityOfFeatured() {
        int quantityOfFeatured = Integer.parseInt(startPage.quantityOfFeatured.getText());
        Assert.assertTrue("Quantity of featured is less than 300",quantityOfFeatured>300);
    }

    @Test
    public void checkSocialAutorisationButtonsIsDisplayed() {
        startPage.signInButton.click();
        Assert.assertTrue("Page don't have Google autorizatin button", logInPage.googleButton.isDisplayed());
        Assert.assertTrue("Page don't have Facebook autorizatin button", logInPage.facebookButton.isDisplayed());
    }

    @Test
    public void checkTopQuestionIsToday(){
        startPage.questionLinksList.get(0).click();
        String testDate = questionPage.publicationDate.getText();
        Assert.assertTrue("This question was asked not today",testDate.contains("today"));
    }

    @Test
    public void checkAwesomeJob () {
        List<WebElement> salaryList = startPage.salaryesList;
        Double higherSalary = 0.0;
        if (salaryList.size() == 0) Assert.assertTrue("There is no jobs with salaries",false);
        else {
            for (WebElement salary: salaryList) {
                String[] salaryArray = salary.toString().replaceAll(",","").split(" - ");

                Double salaryAmount = Double.valueOf(salaryArray[1].substring(1));
                System.out.println(salaryAmount);
                if (salaryAmount>higherSalary) higherSalary=salaryAmount;
            }

            Assert.assertTrue("All salaries are less than 60k",higherSalary>60000);
        }
    }

    @Test
    public void checkLogIn() {
        /*Проверяем залогин в аккаунт по появлению логина в шапке
            1. Кликаем на кнопку логина в шапке
            2. Вводим адрес и пароль
            3. Нажимаем кнопку залогина
            4. Проверяем, что залогинились по наличию аватара
         */
        startPage.logInButton.click();
        logInPage.emailInput.sendKeys("lambru100@yandex.ru");
        logInPage.passwordInput.sendKeys("gfhjkm13");
        logInPage.submitButton.click();
        Assert.assertTrue("We are not logged in - there is no avatar on page",startPage.avatar.isDisplayed());

    }






}
