package figur;

public class Circle extends  Shape {
    public Circle (double x, double y, double size) {
        super(x, y, size);
    }

    @Override
    public double area() {
        return (Math.PI*size*size);
    }


}
