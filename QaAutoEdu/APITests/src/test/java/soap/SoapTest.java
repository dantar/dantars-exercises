package soap;

import net.yandex.speller.services.spellservice.CheckTextRequest;
import net.yandex.speller.services.spellservice.CheckTextResponse;
import net.yandex.speller.services.spellservice.SpellService;
import net.yandex.speller.services.spellservice.SpellServiceSoap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Dantar on 19.02.2017.
 */
public class SoapTest {

    public CheckTextRequest request;
    public SpellService service;
    public SpellServiceSoap port;

    @Before
    public void setUpStartConditions() {
        request = new CheckTextRequest();
        service = new SpellService();
        port = service.getSpellServiceSoap();
    }


    @Test
    public void checkNumberOfGetMistakes() {
        request.setLang("RU");
        request.setText("пиша язик преветствие");
        CheckTextResponse checkTextResponse = port.checkText(request);

        Assert.assertTrue("We don't got three errors", checkTextResponse.getSpellResult().getError().size()==3);
    }


    //Здесь не указан язык - проверяем автоматическое определение
    @Test
    public void checkPosAndRow() {
        request.setText("nagick");
        CheckTextResponse checkTextResponse = port.checkText(request);

        Assert.assertTrue("Position of mistake is wrong",checkTextResponse.getSpellResult().getError().get(0).getPos()==0);
        Assert.assertTrue("Row of mistake is wrong",checkTextResponse.getSpellResult().getError().get(0).getRow()==0);
    }

    @Test
    public void checkAtNextRow() {
        request.setLang("RU");
        request.setText("слово \n ашибка ");
        CheckTextResponse checkTextResponse = port.checkText(request);

        Assert.assertTrue("Row of mistake is wrong",checkTextResponse.getSpellResult().getError().get(0).getRow()==1);
    }

    @Test
    public void checkCorrectionsInPhrase(){
        request.setLang("RU");
        request.setText("Пьеса была сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);

        Assert.assertTrue("Mistake in detected in word 'сиграна'", checkTextResponse.getSpellResult().getError().get(0).getWord().contains("сиграна"));
        Assert.assertTrue("Correction suggest don't contain word 'сыграна'",checkTextResponse.getSpellResult().getError().get(0).getS().contains("сыграна"));
    }

    @Test
    public void checkCorrectionsInWord() {
        request.setLang("RU");
        request.setText("сиграна");
        CheckTextResponse checkTextResponse = port.checkText(request);

        Assert.assertTrue("The word 'сиграна' is not in the error list", checkTextResponse.getSpellResult().getError().get(0).getWord().contains("сиграна"));
        Assert.assertTrue("Amount of suggestions is not 5", checkTextResponse.getSpellResult().getError().get(0).getS().size()==5);
        Assert.assertTrue("Correction suggest don't contain word 'сыграна'", checkTextResponse.getSpellResult().getError().get(0).getS().contains("сыграна"));
    }

}
