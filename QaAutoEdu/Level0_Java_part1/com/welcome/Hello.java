package com.welcome;

import java.util.Scanner;

public class Hello {
    private String name;
    public String setupName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите имя: ");
        name = in.nextLine();
        return name;
    }

    public void welcome() {
        System.out.println("Hello, " + name);
    }

    public void byebye() {
        System.out.println("Bye, " + name);
    }

}
