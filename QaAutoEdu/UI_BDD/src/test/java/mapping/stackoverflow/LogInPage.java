package mapping.stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 19.02.2017.
 */
public class LogInPage {

    private WebDriver driver;

    public LogInPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(className = "google-login")
    public WebElement googleButton;

    @FindBy(className = "facebook-login")
    public WebElement facebookButton;

    @FindBy(id = "email")
    public WebElement emailInput;

    @FindBy (id = "password")
    public WebElement passwordInput;

    @FindBy (id = "submit-button")
    public WebElement submitButton;




}
