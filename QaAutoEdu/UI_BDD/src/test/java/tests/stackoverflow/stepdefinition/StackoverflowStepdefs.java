package tests.stackoverflow.stepdefinition;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.PendingException;

import mapping.stackoverflow.QuestionPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import mapping.stackoverflow.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dantar on 19.02.2017.
 */
public class StackoverflowStepdefs {

    public static StartPage startPage;
    public static LogInPage logInPage;
    public static QuestionPage questionPage;

    public static WebDriver driver;

    @Before("@Stack")
    public void setupStartConditions() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        startPage = new StartPage(driver);
        logInPage = new LogInPage(driver);
        questionPage = new QuestionPage(driver);


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://stackoverflow.com/");

    }

    @After ("@Stack")
    public void quitDriver() {
        driver.quit();
    }

    @Then("^I see quantity of featured$")
    public void iSeeQuantityOfFeatured() throws Throwable {
        int quantityOfFeatured = Integer.parseInt(startPage.quantityOfFeatured.getText());
        Assert.assertTrue("Quantity of featured is less than 300",quantityOfFeatured>300);
    }


    @When("^I go to Sign In Page$")
    public void iGoToSignInPage() throws Throwable {
        startPage.signInButton.click();
    }

    @Then("^I can see social autorization buttons$")
    public void iCanSeeSocialAutorizationButtons() throws Throwable {
        Assert.assertTrue("Page don't have Google autorizatin button", logInPage.googleButton.isDisplayed());
        Assert.assertTrue("Page don't have Facebook autorizatin button", logInPage.facebookButton.isDisplayed());
    }

    @When("^I open first of Top Questions$")
    public void iOpenFirstOfTopQuestions() throws Throwable {
        startPage.questionLinksList.get(0).click();
    }

    @Then("^I can see that it was asked today$")
    public void iCanSeeThatItWasAskedToday() throws Throwable {
        String testDate = questionPage.publicationDate.getText();
        Assert.assertTrue("This question was asked not today",testDate.contains("today"));
    }

    @Then("^I can see job with salary higher than 60000$")
    public void iCanSeeJobWithSalaryHigherThan() throws Throwable {
        List<WebElement> salaryList = startPage.salaryesList;
        Double higherSalary = 0.0;
        if (salaryList.size() == 0) Assert.assertTrue("There is no jobs with salaries",false);
        else {
            for (WebElement salary: salaryList) {
                String[] salaryArray = salary.toString().replaceAll(",","").split(" - ");

                Double salaryAmount = Double.valueOf(salaryArray[1].substring(1));
                System.out.println(salaryAmount);
                if (salaryAmount>higherSalary) higherSalary=salaryAmount;
            }

            Assert.assertTrue("All salaries are less than 60k",higherSalary>60000);
        }
    }

    @When("^I go on log in page$")
    public void iGoOnLogInPage() throws Throwable {
        startPage.logInButton.click();
    }

    @And("^I put email and password in inputs$")
    public void iPutEmailAndPasswordInInputs() throws Throwable {
        logInPage.emailInput.sendKeys("lambru100@yandex.ru");
        logInPage.passwordInput.sendKeys("gfhjkm13");
    }

    @And("^I press Submit button$")
    public void iPressSubmitButton() throws Throwable {
        logInPage.submitButton.click();
    }

    @Then("^I can see my avatar$")
    public void iCanSeeMyAvatar() throws Throwable {
        Assert.assertTrue("We are not logged in - there is no avatar on page",startPage.avatar.isDisplayed());
    }
}
