package rest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Dantar on 19.02.2017.
 */
public class RestTest {

    public HttpClient client;
    public RequestBuilder request;
    public HttpGet get;
    public HttpResponse response;


    public static JSONObject makeJSON (HttpResponse responseHttp) throws IOException {
        String result = "", line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseHttp.getEntity().getContent()));
        while ((line = reader.readLine()) != null) result += line;

        return new JSONObject(result);
    }


    @Before
    public void setStartConditions() {
        client = HttpClientBuilder.create().build();
        request = new RequestBuilder("http://apilayer.net/api/live");
        request.addParameter("access_key", "85fa814028601d1d0833d04d288ec5d4");
    }


    @Test
    public void checkKeysInResponse() throws IOException {
        String[] requiredKeys = {"success","terms","privacy","timestamp","source","quotes"};
        String currencies = "EUR";

        request.addParameter("currencies", currencies);
        String url = request.getUrl();
        get = new HttpGet(url);
        response = client.execute(get);

        String responseJSON = makeJSON(response).toString();

        for(String key: requiredKeys) {
            Assert.assertTrue(key+" is not in response", responseJSON.contains(key));
        }

    }

    @Test
    public void checkQuotesKey() throws IOException {
        String[] currencies = {"EUR","UAH","RUB"};
        String[] requiredQuotes = {"USDEUR","USDUAH","USDRUB"};

        request.addParameterWithManyValues("currencies", currencies);
        String url = request.getUrl();
        get = new HttpGet(url);
        response = client.execute(get);

        JSONObject quotes = makeJSON(response).getJSONObject("quotes");

        for (String requiredQuote: requiredQuotes){
            Assert.assertTrue("Quotes key does not contain " + requiredQuote + " key",quotes.names().toString().contains(requiredQuote));
        }

        Assert.assertTrue("Quotes key doesn't contain exactly USDEUR, USDUAH, USDRUB keys",quotes.names().length() == requiredQuotes.length);
    }

    @Test
    public void checkNumberOfCurrencies() throws IOException {
        String currencies = "";

        request.addParameter("currencies", currencies);
        String url = request.getUrl();
        get = new HttpGet(url);
        HttpResponse response = client.execute(get);

        JSONObject quotes = makeJSON(response).getJSONObject("quotes");

        Assert.assertTrue("Number of all currencies is not equal to 169", quotes.length() == 169);
    }

    @Test
    public void checkRatioUSDEUR() throws IOException {
        String currencies = "EUR";

        request.addParameter("currencies", currencies);
        String url = request.getUrl();
        get = new HttpGet(url);
        HttpResponse response = client.execute(get);

        JSONObject quotes = makeJSON(response).getJSONObject("quotes");
        Double quotesUSDEUR = new Double(quotes.get("USDEUR").toString());

        Assert.assertTrue("USD / EUR ratio is not between 0.9-0.99", quotesUSDEUR >=0.9 && quotesUSDEUR <=0.99);
    }

    @Test
    public void checkError105() throws IOException {
        String currencies = "UAH";

        request.addParameter("source", currencies);
        String url = request.getUrl();
        get = new HttpGet(url);
        response = client.execute(get);

        Assert.assertTrue("Don't get the 105 error",makeJSON(response).toString().contains("105"));
    }


}
