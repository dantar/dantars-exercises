package testpack;

import org.junit.*;
import oop.*;

public class TestSquare {
    public static Square square;

    @Before
    public void createSquare() {
        square = new Square(0, 0, 10);
    }

    @Test
    // Проверка наследования
    public void checkInstance() {
        Assert.assertTrue(square instanceof Shape);
    }

    @Test
    // Проверка вычисления площади
    public void checkSquareArea() {
        double squareArea = square.area();
        Assert.assertEquals("Ожидаемый результат был 100.0, но текущий результат " + squareArea, 100.0, squareArea, 0.1);
    }

    @Test
    // Проверка перемещения
    public void checkSquareMove() {
        square.move(3,7);
        double[] realCoord = {square.getX(), square.getY()};
        double[] expCoord = {3,7};
        Assert.assertArrayEquals("Координаты не совпадают",expCoord,realCoord,0.1);
    }

    @Test
    // Проверка изменения размера
    public void checkSquareResize() {
        square.resize(0.5);
        double resizedRadius = square.getSize();
        Assert.assertEquals("Неверный размер " + resizedRadius, 5.0, resizedRadius, 0.1);
    }

    @After
    public void cleanUp() {
        square = null;
    }


}
