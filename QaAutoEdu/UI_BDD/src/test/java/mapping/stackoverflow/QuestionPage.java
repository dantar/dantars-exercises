package mapping.stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 19.02.2017.
 */
public class QuestionPage {

    private WebDriver driver;

    public QuestionPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//*[@id='qinfo']/tbody/tr[1]/td[2]")
    public WebElement publicationDate;




}
