package mapping.amazone;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 19.02.2017.
 */
public class LogInPage {

    private WebDriver driver;

    public LogInPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (id = "nav-link-accountList")
    public WebElement goToLogInPageButton;

    @FindBy (id = "ap_email")
    public WebElement emailInput;

    @FindBy (id = "ap_password")
    public WebElement passwordInput;

    @FindBy (id = "signInSubmit")
    public WebElement signInButton;

    @FindBy (css = "#nav-link-accountList .nav-line-1")
    public WebElement greeting;


}
