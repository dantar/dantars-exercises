package cucetestpackage.stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import oop.*;
import org.junit.Assert;

/**
 * Created by garkusha on 10.01.2017.
 */
public class oopStepdefs {
    private Circle circle;
    private Square square;
    private double[] actCoordinates = new double[2];
    private double[] expCoordinates = new double[2];
    private double newSize;

    @Given("^I create Circle with coordinates \"([^\"]*)\",\"([^\"]*)\" and size \"([^\"]*)\"$")
    public void iCreateCircleWithCoordinatesAndSize(String arg0, String arg1, String arg2) throws Throwable {
        circle = new Circle(Double.valueOf(arg0), Double.valueOf(arg1), Double.valueOf(arg2));
    };

    @Then("^Circle is instance of Shape$")
    public void circleIsInstanceOfShape() throws Throwable {
        Assert.assertTrue(circle instanceof Shape);
    }

    @Then("^Area of Circle equals (.+)$")
    public void areaOfCircleEquals(double arg0) throws Throwable {
        Assert.assertEquals(arg0, circle.area(), 0.2);
    }

    @When("^I move Circle by (.+),(.+)$")
    public void iMoveCircleBy(double arg0, double arg1) throws Throwable {
        circle.move(arg0,arg1);
        actCoordinates[0] = circle.getX();
        actCoordinates[1] = circle.getY();
    }

    @Then("^Coordinates of Circle equals (.+),(.+)$")
    public void coordinatesOfCircleEquals(double arg0, double arg1) throws Throwable {
        expCoordinates[0] = arg0;
        expCoordinates[1] = arg1;
        Assert.assertArrayEquals(expCoordinates,actCoordinates,0.1);
    }

    @When("^I resize circle by coefficient (.+)$")
    public void iResizeCircleByCoefficient(double arg0) throws Throwable {
        circle.resize(arg0);
        newSize = circle.getSize();
    }

    @Then("^Size of Circle equals (.+)$")
    public void sizeOfCircleEquals(double arg0) throws Throwable {
        Assert.assertTrue(newSize == arg0);
    }

    @Given("^I create Square with coordinates \"([^\"]*)\", \"([^\"]*)\" and size \"([^\"]*)\"$")
    public void iCreateSquareWithCoordinatesAndSize(String arg0, String arg1, String arg2) throws Throwable {
        square = new Square(Double.valueOf(arg0), Double.valueOf(arg1), Double.valueOf(arg2));
    }

    @Then("^Square is instance of Shape$")
    public void squareIsInstanceOfShape() throws Throwable {
        Assert.assertTrue(square instanceof Shape);
    }

    @Then("^Area of Square equals (.+)$")
    public void areaOfSquareEquals(double arg0) throws Throwable {
        Assert.assertEquals(arg0, square.area(), 0.2);
    }

    @When("^I move Square by (.+),(.+)$")
    public void iMoveSquareBy(double arg0, double arg1) throws Throwable {
        square.move(arg0,arg1);
        actCoordinates[0] = square.getX();
        actCoordinates[1] = square.getY();
    }

    @Then("^Coordinates of Square equals (.+),(.+)$")
    public void coordinatesOfSquareEquals(double arg0, double arg1) throws Throwable {
        expCoordinates[0] = arg0;
        expCoordinates[1] = arg1;
        Assert.assertArrayEquals(expCoordinates,actCoordinates,0.1);
    }

    @When("^I resize square by coefficient (.+)$")
    public void iResizeSquareByCoefficient(double arg0) throws Throwable {
        square.resize(arg0);
        newSize = square.getSize();
    }

    @Then("^Size of Square equals (.+)$")
    public void sizeOfSquareEquals(double arg0) throws Throwable {
        Assert.assertTrue(newSize == arg0);
    }

}
