package com.main;

import games.*;

import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Integer number = 0;
        String answer;
        Double base = 0.0;
        Double pow = 0.0;

// Матрица заданного размера
        System.out.println("\n Создание матрицы заданного размера");

        while (true) {
            System.out.println("Введите число от 1 до 9. Если не желаете создавать матрицу, введите 'quit' ");
            answer = in.next();
            if (Objects.equals(answer, "quit")) break;

            try {
                number = Integer.parseInt(answer);
                if (number > 0 && number < 10) {
                    Integer [][] array = Matrix.buildMatrix( number );
                    for (int i=0; i<number; i++){
                        for (int j=0; j<number; j++){
                            System.out.printf("%2d",array[i][j]);
                        }
                        System.out.println();
                    }
                    System.out.println("\nЕсли желаете повторить, введите 'repeat'. Введите любой другой ответ, чтобы выйти");
                    answer = in.next();
                    if (!Objects.equals(answer, "repeat")) break;
                }
                else System.out.println("Число не в ходит в интервал от 1 до 9");

            } catch (NumberFormatException e) {
                System.err.println("Некорректный ввод");
            }
        }

// Произведение массива
        System.out.println("\n Вычисление произведения массива");

        while (true) {
            System.out.println("Введите размер массива от 1 до 20. Для пропуска этого метода введите 'quit' ");
            answer = in.next();
            if (Objects.equals(answer, "quit")) {
                break;
            }
            try {
                number = Integer.parseInt(answer);
                if (number > 0 && number < 20) {
                    break;
                }
                else System.out.println("Число не в ходит в интервал от 1 до 20");
            } catch (NumberFormatException e) {
                System.err.println("Некорректный ввод");
            }
        }

        if (!Objects.equals(answer, "quit")) {
            Integer array [] = new Integer[number];
            for (int i=0; i < number; i++){
                System.out.println("Введите элемент массива номер " + (i+1));
                try {
                    answer = in.next();
                    array [i] = Integer.parseInt(answer);
                } catch (NumberFormatException e) {
                    System.err.println("Некорректный ввод");
                    i--;
                }
            }
            System.out.println("Произведение введённого массива равно " + calculator.Arithmetic.arrayMultiplication(array) );
        }

// Возведение в степень
        System.out.println("\n Возведение числа в степень");

        while (true) {
            System.out.println("Введите число, которое будет возводиться в степень. Для пропуска этого метода введите 'quit' ");
            answer = in.next();
            if (Objects.equals(answer, "quit")) {
                break;
            }
            try {
                base = Double.parseDouble (answer);
                break;
                } catch (NumberFormatException e) {
                System.err.println("Некорректный ввод");
            }
        }

        if (!Objects.equals(answer, "quit")) {
            while (true) {
                System.out.println("Введите степень, в которую будет возводиться число. Для пропуска этого метода введите 'quit' ");
                answer = in.next();
                if (Objects.equals(answer, "quit")) {
                    break;
                }
                try {
                    pow = Double.parseDouble (answer);
                    break;
                } catch (NumberFormatException e) {
                    System.err.println("Некорректный ввод");
                }
            }
            System.out.println("Результат возведения числа в степень равен " + calculator.Arithmetic.power(base,pow) );
        }

// Деление
        System.out.println("\n Деление");

        while (true) {
            System.out.println("Введите число, которое будет делиться. Для пропуска этого метода введите 'quit' ");
            answer = in.next();
            if (Objects.equals(answer, "quit")) {
                break;
            }
            try {
                base = Double.parseDouble (answer);
                break;
            } catch (NumberFormatException e) {
                System.err.println("Некорректный ввод");
            }
        }

        if (!Objects.equals(answer, "quit")) {
            while (true) {
                System.out.println("Введите делитель. Для пропуска этого метода введите 'quit' ");
                answer = in.next();
                if (Objects.equals(answer, "quit")) {
                    break;
                }
                try {
                    pow = Double.parseDouble (answer);
                    if (pow !=0.0) break;
                    else System.err.println("Некорректный ввод");
                } catch (NumberFormatException e) {
                    System.err.println("Некорректный ввод");
                }
            }
            System.out.println("Результат деления равен  " + calculator.Arithmetic.division(base,pow) );
        }

// Квадратный корень
        System.out.println("\n Квадратный корень");

        while (true) {
            System.out.println("Введите число для извлечения квадратного корня. Для пропуска этого метода введите 'quit' ");
            answer = in.next();
            if (Objects.equals(answer, "quit")) {
                break;
            }
            try {
                base = Double.parseDouble (answer);
                if (base > 0) {
                    System.out.println("Квадратный корень равен " + calculator.Arithmetic.root(base));
                    break;
                }
                else System.err.println("Некорректный ввод");
            } catch (NumberFormatException e) {
                System.err.println("Некорректный ввод");
            }
        }

// Улитка
        System.out.println("\n Улитка");

        Snail spiral = new Snail();

        while (true) {
            System.out.println("Введите длину стороны квадратной матрицы, она должна быть не меньше 3. Для пропуска этого метода, введите 'quit' ");
            answer = in.next();
            if (Objects.equals(answer, "quit")) break;

            try {
                number = Integer.parseInt(answer);
                if (number > 2) {
                    Integer[][] snailMatrix = spiral.calculateSnail(number);
                    for (int i=0; i<number; i++){
                        for (int j=0; j<number; j++){
                            System.out.printf("%4d",snailMatrix[i][j]);
                        }
                        System.out.println();
                    }

                    System.out.println("\nЕсли желаете повторить, введите 'repeat'. Введите любой другой ответ, чтобы выйти");
                    answer = in.next();
                    if (!Objects.equals(answer, "repeat")) break;
                }
                else System.out.println("Число меньше 3");

            } catch (NumberFormatException e) {
                System.err.println("Некорректный ввод");
            }
        }

//Слово-палиндром

        System.out.println("\n Проверка, является ли слово палиндромом");

        Palindrome pal = new Palindrome();

        while (true) {
            System.out.println("Введите слово, чтобы проверить, полиндром ли оно. Для пропуска этого метода, введите 'quit' ");
            answer = in.next();
            if (answer.replaceAll(" ","").length() < 3) {
                System.err.println("Некорректный ввод, либо слишком короткое слово");
                continue;
            }

            if (Objects.equals(answer, "quit")) break;

            if (pal.checkWord(answer)) System.out.println("Слово " + answer + " является палиндромом");
            else System.out.println("Слово " + answer + " не является палиндромом");

            System.out.println("\nЕсли желаете повторить, введите 'repeat'. Введите любой другой ответ, чтобы выйти");
            answer = in.next();
            if (!Objects.equals(answer, "repeat")) break;
        }

//Фраза-палиндром

        System.out.println("\n Проверка, является ли фраза палиндромом");
        Scanner in2 = new Scanner(System.in);

        while (true) {
            System.out.println("Введите фразу, чтобы проверить, является ли она палиндромом. Для пропуска этого метода, введите 'quit' ");
            answer = in2.nextLine();
            if (answer.replaceAll(" ","").length() < 3) {
                System.err.println("Некорректный ввод, либо слишком короткая фраза");
                continue;
            }

            if (Objects.equals(answer, "quit")) break;

            if (pal.checkPhrase(answer)) System.out.println("Фраза " + answer + " является палиндромом");
            else System.out.println("Фраза " + answer + " не является палиндромом");

            System.out.println("\nЕсли желаете повторить, введите 'repeat'. Введите любой другой ответ, чтобы выйти");
            answer = in2.nextLine();
            if (!Objects.equals(answer, "repeat")) break;
        }

    }
}
