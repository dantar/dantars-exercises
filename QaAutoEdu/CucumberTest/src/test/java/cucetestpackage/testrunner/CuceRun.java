package cucetestpackage.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by garkusha on 19.01.2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/nixreport", "json:target/cucumber.json"},
        features = "src/test/java/cucetestpackage/feature",
        glue ="cucetestpackage.stepdefinition",
        tags = {"@Matrix,@oop"}
)

public class CuceRun {
}
