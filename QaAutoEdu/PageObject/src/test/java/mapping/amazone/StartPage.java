package mapping.amazone;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 18.02.2017.
 */
public class StartPage {
    private WebDriver driver;

    public StartPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (id = "twotabsearchtextbox")
    public WebElement searchInput;

    @FindBy (xpath = ".//span[@id='nav-search-submit-text']/following-sibling::input")
    public WebElement searchButton;

    @FindBy (id = "nav-link-accountList")
    public WebElement signInButton;



}
