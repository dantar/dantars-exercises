@Stack
Feature: Stackoverflow

  Scenario: 001 When I am on the main page, I can see that a quantity of featured is more than 300
    Then I see quantity of featured

  Scenario: 002 When I go to Sign In Page, I can see social autorization buttons
    When I go to Sign In Page
    Then I can see social autorization buttons

  Scenario: 003 When I open first of Top Questions, I can see that it was asked today
    When I open first of Top Questions
    Then I can see that it was asked today

  Scenario: 004 When I am on the main page, I can see job with salary higher than 60000$
    Then I can see job with salary higher than 60000

  Scenario: 005 When I am logged in, I can see my avatar
    When I go on log in page
    And I put email and password in inputs
    And I press Submit button
    Then I can see my avatar
