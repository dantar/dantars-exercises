package calculator;

import static java.lang.Integer.*;
import static java.lang.Math.*;

public class Arithmetic {
    public static Integer arrayMultiplication(Integer array[]){
        Integer answer = 1;
        for (int i=0; i < array.length; i++){
            answer *= array[i];
        }
        return answer;
    }


    public static Double power(Double base, Double pow){
        return Math.pow(base, pow);
    }

    public static Double division(Double victim, Double divider){
        return victim/divider;
    }

    public static Double root(Double number){
        return sqrt(number);
    }
}


