package webdrivertest.amazone;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.SocketPermission;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import mapping.amazone.*;


/**
 * Created by Dantar on 11.02.2017.
 */
public class TestAmazone {

    public static StartPage startPage;
    public static SearchResultsPage searchResultsPage;
    public static MerchPage merchPage;
    public static CartPage cartPage;
    public static LogInPage logInPage;

    public static WebDriver driver;

    @Before
    public void setupStartConditions() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        startPage = new StartPage(driver);
        searchResultsPage = new SearchResultsPage(driver);
        merchPage = new MerchPage(driver);
        cartPage = new CartPage(driver);
        logInPage = new LogInPage(driver);
        
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");
    }

    @After
     public void quitDriver() {
        driver.quit();
    }

    @Test
    public void checkAllResultsHaveWord() {
        //В поле поиска вводим ключевое слово: 'duck' и нажимаем значок поиска (лупу)
        String searchWord = "duck";
        startPage.searchInput.sendKeys(searchWord);
        startPage.searchButton.click();

        //Проверяем. что каждый из результатов содержит ключевое слово (в том числе в другом регистре)
        for (WebElement searchResult: searchResultsPage.searchResults){
            String searchResultText = searchResult.getText().toLowerCase();
            Assert.assertTrue("Search result " + searchResultText + " does not contain substring" + searchWord, searchResultText.contains(searchWord));
        }
    }

    @Test
    public void checkNumberOfResults() {
        //В поле поиска вводим ключевое слово: 'duck' и нажимаем значок поиска (лупу)
        String searchWord = "duck";
        startPage.searchInput.sendKeys(searchWord);
        startPage.searchButton.click();

        //Запоминаем количество найденных результатов.
        Integer firstQuantity = searchResultsPage.getNumberOfSearchResults();

        //В блоке 'Shop by Category' кликаем на 'Baby Products'.
        searchResultsPage.babyProductsCategoryButton.click();

        //На загруженной странице проверяем, что результатов стало меньше для более целевой категории, чем было в общем поиске по одному слову.
        Integer sortQuantity = searchResultsPage.getNumberOfSearchResults();
        Assert.assertTrue("Number of search results in target category - " + sortQuantity + " isn't less than number of total results - "+ firstQuantity, firstQuantity >= sortQuantity);
    }

    @Test
    public void checkAddMerchToCart() {
        //В поле поиска вводим ключевое слово: 'knife kitchen' и нажимаем значок поиска (лупу)
        String searchWord = "knife kitchen";
        startPage.searchInput.sendKeys(searchWord);
        startPage.searchButton.click();

        //В результатах поиска находим любой 8-ми дюймовый нож (8 Inch). Открываем страницу с его листингом(описанием).
        searchResultsPage.eightInchKnife.click();

        //Добавляем в корзину (Add to Cart). Запоминаем название и цену.
        String knifePriceString = merchPage.merchPrice.getText();
        Double knifePrice = merchPage.getMerchPrice();
        String knifeName = merchPage.merchName.getText();
        merchPage.addToCartButton.click();

        //Производим поиск по ключевому слову 'duck'
        searchWord = "duck";
        startPage.searchInput.sendKeys(searchWord);
        startPage.searchButton.click();

        //Выбираем первый результат. Запоминаем название и цену. Добавляем в корзину.
        searchResultsPage.searchResults.get(0).click();
        String duckPriceString = merchPage.merchPrice.getText();
        Double duckPrice = merchPage.getMerchPrice();
        String duckName = merchPage.merchName.getText();
        merchPage.addToCartButton.click();

        //Переходим в корзину (справа вверху кликаем значок  "тележка").
        merchPage.goToCartButton.click();

        //Проверяем, что в корзине только 2 наших товара, которые мы выбрали, и цена совпдает. Првоеряем, что общая сумма покупки равна сумме цен двух выбранных товаров.
        List<WebElement> namesInCartList = cartPage.mercNamesInCart;
        String[] namesInCartArray = {namesInCartList.get(0).getText(), namesInCartList.get(1).getText()} ;

        List<WebElement> pricesInCartList = cartPage.mercPricesInCart;
        String[] pricesInCartArray = {pricesInCartList.get(0).getText(), pricesInCartList.get(1).getText()};

        Double totalPrice = cartPage.getTotalMerchPrice();

        Assert.assertEquals("Number of products is incorrect", 2, namesInCartList.size());
        Assert.assertTrue("First name is incorrect",Arrays.asList(namesInCartArray).contains(knifeName));
        Assert.assertTrue("Second name is incorrect",Arrays.asList(namesInCartArray).contains(duckName));
        Assert.assertTrue("First price is incorrect",Arrays.asList(pricesInCartArray).contains(knifePriceString));
        Assert.assertTrue("Second price is incorrect",Arrays.asList(pricesInCartArray).contains(duckPriceString));
        Assert.assertEquals("Total price is incorrect", totalPrice,knifePrice+duckPrice, 0.1);
    }

    @Test
    public void checkDeleteMerchFromCart() {
        //Находим и добавляем в корзину 3 любых товара.
        int searchSize=3;
        String[] searchWords = {"Hand Forged Iron Medieval Shield Umbo Fully Functional 12g", "Szco Supplies Brass Crusader Helmet", "Szco Supplies Studded Battle Axe"};
        String[] namesArray = new String[searchSize];

        for (int i=0; i<searchSize; i++) {
            startPage.searchInput.sendKeys(searchWords[i]);
            startPage.searchButton.click();
            searchResultsPage.searchResults.get(0).click();
            namesArray[i] = merchPage.merchName.getText();
            merchPage.addToCartButton.click();
        }

        //Заходим в корзину и убеждаемся. что там именно наши выбранные товары (по названию).
        merchPage.goToCartButton.click();
        List<WebElement> namesInCartList = cartPage.mercNamesInCart;
        String[] namesInCartArray = {namesInCartList.get(0).getText(),namesInCartList.get(1).getText(),namesInCartList.get(2).getText()};

        Assert.assertTrue("Quantity of merch in cart is incorrect",namesInCartList.size()==searchSize);

        Assert.assertTrue(namesArray[0]+" isn't at cart", Arrays.asList(namesInCartArray).contains(namesArray[0]));
        Assert.assertTrue(namesArray[1]+" isn't at cart", Arrays.asList(namesInCartArray).contains(namesArray[1]));
        Assert.assertTrue(namesArray[2]+" isn't at cart", Arrays.asList(namesInCartArray).contains(namesArray[2]));

        //Удаляем второй товар из списка.
        cartPage.deleteButtons.get(1).click();
        driver.get(driver.getCurrentUrl());

        //Проверяем, что в корзине остались два других товара, а удаленный исчез со страницы.
        List<WebElement> newNamesInCartList = cartPage.mercNamesInCart;
        String[] newNamesInCartArray = {newNamesInCartList.get(0).getText(),newNamesInCartList.get(1).getText()};

        Assert.assertTrue("Quantity of merch in cart is incorrect",newNamesInCartList.size()==2);

        Assert.assertTrue(namesInCartArray[0]+" isn't at cart", Arrays.asList(newNamesInCartArray).contains(namesInCartArray[0]));
        Assert.assertFalse(namesInCartArray[1]+" is at cart", Arrays.asList(newNamesInCartArray).contains(namesInCartArray[1]));
        Assert.assertTrue(namesInCartArray[2]+" isn't at cart", Arrays.asList(newNamesInCartArray).contains(namesInCartArray[2]));

    }

    @Test
    public void checkLogIn() {
        //Проверяем залогин в аккаунт по появлению логина в шапке
        //  1. Кликаем на кнопку логина в шапке
        //  2. Вводим адрес и пароль
        //  3. Нажимаем кнопку залогина
        //  4. Проверяем, что в шапке появилось имя

        logInPage.goToLogInPageButton.click();
        logInPage.emailInput.sendKeys("lambru100@yandex.ru");
        logInPage.passwordInput.sendKeys("gfhjkm13");
        logInPage.signInButton.click();
        String greeting = logInPage.greeting.getText();

        Assert.assertTrue("you are not logged in", greeting.contains("lambru"));
    }

}
