package testpack;

import games.Matrix;
import org.junit.*;

import java.util.regex.Matcher;

public class testMatrix {


    @Test
    // Проверка создания матрицы
    public void checkMatrixIsNotNull() {
        Integer[][] matrixA = Matrix.buildMatrix(5);
        Assert.assertNotNull(matrixA);
    }

    @Test
    // Проверка размера матрицы
    public void checkIncorrectInput() {
        Integer[][] matrixA = Matrix.buildMatrix(5);
        int length = matrixA.length;
        Assert.assertEquals("Размер стороны матрицы должен быть 5, но он равен" + length,length,5);
    }

    @Test
    // Проверка соответствия содержимого ожиданиям
    public void checkMatrixOfSize5() {
        Integer[][] actMatrix = Matrix.buildMatrix(5);
        int[][] expMatrix = {
                {1,2,3,4,5},
                {6,7,8,9,1},
                {2,3,4,5,6},
                {7,8,9,1,2},
                {3,4,5,6,7}
        };
        Assert.assertArrayEquals("Содержимое матрицы не соответствует правилу",expMatrix,actMatrix);
    }

}
