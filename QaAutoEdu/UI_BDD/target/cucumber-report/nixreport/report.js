$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("stackoverflow.feature");
formatter.feature({
  "line": 2,
  "name": "Stackoverflow",
  "description": "",
  "id": "stackoverflow",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Stack"
    }
  ]
});
formatter.before({
  "duration": 5719280524,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "001 When I am on the main page, I can see that a quantity of featured is more than 300",
  "description": "",
  "id": "stackoverflow;001-when-i-am-on-the-main-page,-i-can-see-that-a-quantity-of-featured-is-more-than-300",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I see quantity of featured",
  "keyword": "Then "
});
formatter.match({
  "location": "StackoverflowStepdefs.iSeeQuantityOfFeatured()"
});
formatter.result({
  "duration": 99348234,
  "status": "passed"
});
formatter.after({
  "duration": 768122201,
  "status": "passed"
});
formatter.before({
  "duration": 4952116652,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "002 When I go to Sign In Page, I can see social autorization buttons",
  "description": "",
  "id": "stackoverflow;002-when-i-go-to-sign-in-page,-i-can-see-social-autorization-buttons",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I go to Sign In Page",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I can see social autorization buttons",
  "keyword": "Then "
});
formatter.match({
  "location": "StackoverflowStepdefs.iGoToSignInPage()"
});
formatter.result({
  "duration": 822944252,
  "status": "passed"
});
formatter.match({
  "location": "StackoverflowStepdefs.iCanSeeSocialAutorizationButtons()"
});
formatter.result({
  "duration": 62851299,
  "status": "passed"
});
formatter.after({
  "duration": 622248304,
  "status": "passed"
});
formatter.before({
  "duration": 4917707743,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "003 When I open first of Top Questions, I can see that it was asked today",
  "description": "",
  "id": "stackoverflow;003-when-i-open-first-of-top-questions,-i-can-see-that-it-was-asked-today",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 12,
  "name": "I open first of Top Questions",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I can see that it was asked today",
  "keyword": "Then "
});
formatter.match({
  "location": "StackoverflowStepdefs.iOpenFirstOfTopQuestions()"
});
formatter.result({
  "duration": 1578446440,
  "status": "passed"
});
formatter.match({
  "location": "StackoverflowStepdefs.iCanSeeThatItWasAskedToday()"
});
formatter.result({
  "duration": 38955403,
  "status": "passed"
});
formatter.after({
  "duration": 604457166,
  "status": "passed"
});
formatter.before({
  "duration": 4990167033,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "004 When I am on the main page, I can see job with salary higher than 60000$",
  "description": "",
  "id": "stackoverflow;004-when-i-am-on-the-main-page,-i-can-see-job-with-salary-higher-than-60000$",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "I can see job with salary higher than 60000",
  "keyword": "Then "
});
formatter.match({
  "location": "StackoverflowStepdefs.iCanSeeJobWithSalaryHigherThan()"
});
formatter.result({
  "duration": 20023972394,
  "error_message": "java.lang.AssertionError: There is no jobs with salaries\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat tests.stackoverflow.stepdefinition.StackoverflowStepdefs.iCanSeeJobWithSalaryHigherThan(StackoverflowStepdefs.java:85)\r\n\tat ✽.Then I can see job with salary higher than 60000(stackoverflow.feature:16)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 648538216,
  "status": "passed"
});
formatter.before({
  "duration": 4968544686,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "005 When I am logged in, I can see my avatar",
  "description": "",
  "id": "stackoverflow;005-when-i-am-logged-in,-i-can-see-my-avatar",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 19,
  "name": "I go on log in page",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I put email and password in inputs",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I press Submit button",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I can see my avatar",
  "keyword": "Then "
});
formatter.match({
  "location": "StackoverflowStepdefs.iGoOnLogInPage()"
});
formatter.result({
  "duration": 875812600,
  "status": "passed"
});
formatter.match({
  "location": "StackoverflowStepdefs.iPutEmailAndPasswordInInputs()"
});
formatter.result({
  "duration": 146862344,
  "status": "passed"
});
formatter.match({
  "location": "StackoverflowStepdefs.iPressSubmitButton()"
});
formatter.result({
  "duration": 78538448,
  "status": "passed"
});
formatter.match({
  "location": "StackoverflowStepdefs.iCanSeeMyAvatar()"
});
formatter.result({
  "duration": 1622759393,
  "status": "passed"
});
formatter.after({
  "duration": 614265738,
  "status": "passed"
});
});