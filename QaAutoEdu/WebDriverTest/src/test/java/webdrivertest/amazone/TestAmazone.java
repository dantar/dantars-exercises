package webdrivertest.amazone;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.SocketPermission;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * Created by Dantar on 11.02.2017.
 */
public class TestAmazone {

    WebDriver driver;

    @Before
    public void setupStartConditions() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");
    }

    @After
     public void quitDriver() {
        driver.quit();
    }

    @Test
    public void checkAllResultsHaveWord() {
        //В поле поиска вводим ключевое слово: 'duck' и нажимаем значок поиска (лупу)
        String searchWord = "duck";
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchWord);
        driver.findElement(By.xpath(".//span[@id='nav-search-submit-text']/following-sibling::input")).click();

        //Проверяем. что каждый из результатов содержит ключевое слово (в том числе в другом регистре)
        List<WebElement> searchResults = driver.findElements(By.xpath(".//h2[contains(@class, 's-access-title')]"));
        for (WebElement searchResult: searchResults){
            String searchResultText = searchResult.getText().toLowerCase();
            Assert.assertTrue("Search result " + searchResultText + " does not contain substring" + searchWord, searchResultText.contains(searchWord));
        }
    }

    @Test
    public void checkNumberOfResults() {
        //В поле поиска вводим ключевое слово: 'duck' и нажимаем значок поиска (лупу)
        String searchWord = "duck";
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchWord);
        driver.findElement(By.xpath(".//span[@id='nav-search-submit-text']/following-sibling::input")).click();

        //Запоминаем количество найденных результатов.
        String textWithCounter = driver.findElement(By.id("s-result-count")).getText();
        String[] arrayWithCounter = textWithCounter.split(" ");
        Integer firstQuantity = Integer.parseInt(arrayWithCounter[2].replace(",",""));

        //В блоке 'Shop by Category' кликаем на 'Baby Products'.
        driver.findElement(By.cssSelector(".cell2 .acs-mn2-cellTitle")).click();

        //На загруженной странице проверяем, что результатов стало меньше для более целевой категории, чем было в общем поиске по одному слову.
        textWithCounter = driver.findElement(By.id("s-result-count")).getText();
        arrayWithCounter = textWithCounter.split(" ");
        Integer sortQuantity = Integer.parseInt(arrayWithCounter[2].replace(",",""));
        Assert.assertTrue("Number of search results in target category - " + sortQuantity + " isn't less than number of total results - "+ firstQuantity, firstQuantity >= sortQuantity);
    }

    @Test
    public void checkAddMerchToCart() {
        //В поле поиска вводим ключевое слово: 'knife kitchen' и нажимаем значок поиска (лупу)
        String searchWord = "knife kitchen";
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchWord);
        driver.findElement(By.xpath(".//span[@id='nav-search-submit-text']/following-sibling::input")).click();

        //В результатах поиска находим любой 8-ми дюймовый нож (8 Inch). Открываем страницу с его листингом(описанием).
        driver.findElement(By.xpath(".//h2[contains(@class, 's-access-title') and contains(text(), '8 Inch')]")).click();

        //Добавляем в корзину (Add to Cart). Запоминаем название и цену.
        String knifePriceString = driver.findElement(By.xpath(".//span[contains(@class, 'a-color-price')]")).getText();
        Double knifePrice = Double.valueOf(knifePriceString.replace("$",""));
        String knifeName = driver.findElement(By.id("productTitle")).getText();
        driver.findElement(By.id("add-to-cart-button")).click();

        //Производим поиск по ключевому слову 'duck'
        searchWord = "duck";
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchWord);
        driver.findElement(By.xpath(".//span[@id='nav-search-submit-text']/following-sibling::input")).click();

        //Выбираем первый результат. Запоминаем название и цену. Добавляем в корзину.
        driver.findElement(By.xpath(".//h2[contains(@class, 's-access-title')]")).click();
        String duckPriceString = driver.findElement(By.xpath(".//span[contains(@class, 'a-color-price')]")).getText();
        Double duckPrice = Double.valueOf(duckPriceString.replace("$",""));
        String duckName = driver.findElement(By.id("productTitle")).getText();
        driver.findElement(By.id("add-to-cart-button")).click();

        //Переходим в корзину (справа вверху кликаем значок  "тележка").
        driver.findElement(By.id("nav-cart")).click();

        //Проверяем, что в корзине только 2 наших товара, которые мы выбрали, и цена совпдает. Првоеряем, что общая сумма покупки равна сумме цен двух выбранных товаров.
        List<WebElement> namesInCartList = driver.findElements(By.xpath(".//div[contains(@class, 'sc-list-item-content')]//span[contains(@class, 'sc-product-title')]"));
        String[] namesInCartArray = {namesInCartList.get(0).getText(), namesInCartList.get(1).getText()} ;

        List<WebElement> pricesInCartList = driver.findElements(By.xpath(".//div[contains(@class, 'sc-list-item-content')]//span[contains(@class, 'a-color-price')]"));
        String[] pricesInCartArray = {pricesInCartList.get(0).getText(), pricesInCartList.get(1).getText()};

        Double totalPrice = Double.valueOf(driver.findElement(By.xpath(".//div[contains(@class, 'sc-subtotal')]//span[contains(@class,'sc-price')]")).getText().replace("$",""));

        Assert.assertEquals("Number of products is incorrect", 2, namesInCartList.size());
        Assert.assertTrue("First name is incorrect",Arrays.asList(namesInCartArray).contains(knifeName));
        Assert.assertTrue("Second name is incorrect",Arrays.asList(namesInCartArray).contains(duckName));
        Assert.assertTrue("First price is incorrect",Arrays.asList(pricesInCartArray).contains(knifePriceString));
        Assert.assertTrue("Second price is incorrect",Arrays.asList(pricesInCartArray).contains(duckPriceString));
        Assert.assertEquals("Total price is incorrect", totalPrice,knifePrice+duckPrice, 0.1);
    }

    @Test
    public void checkDeleteMerchFromCart() {
        //Находим и добавляем в корзину 3 любых товара.
        int searchSize=3;
        String[] searchWords = {"Hand Forged Iron Medieval Shield Umbo Fully Functional 12g", "Szco Supplies Brass Crusader Helmet", "Szco Supplies Studded Battle Axe"};
        String[] namesArray = new String[searchSize];

        for (int i=0; i<searchSize; i++) {
            driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchWords[i]);
            driver.findElement(By.xpath(".//span[@id='nav-search-submit-text']/following-sibling::input")).click();
            driver.findElement(By.xpath(".//h2[contains(@class, 's-access-title')]")).click();
            namesArray[i] = driver.findElement(By.id("productTitle")).getText();
            driver.findElement(By.id("add-to-cart-button")).click();
        }

        //Заходим в корзину и убеждаемся. что там именно наши выбранные товары (по названию).
        driver.findElement(By.id("nav-cart")).click();
        List<WebElement> namesInCartList = driver.findElements(By.xpath(".//div[contains(@class, 'sc-list-item-content')]//span[contains(@class, 'sc-product-title')]"));

        String[] namesInCartArray = {namesInCartList.get(0).getText(),namesInCartList.get(1).getText(),namesInCartList.get(2).getText()};

        Assert.assertTrue("Quantity of merch in cart is incorrect",namesInCartList.size()==searchSize);

        Assert.assertTrue(namesArray[0]+" isn't at cart", Arrays.asList(namesInCartArray).contains(namesArray[0]));
        Assert.assertTrue(namesArray[1]+" isn't at cart", Arrays.asList(namesInCartArray).contains(namesArray[1]));
        Assert.assertTrue(namesArray[2]+" isn't at cart", Arrays.asList(namesInCartArray).contains(namesArray[2]));

        //Удаляем второй товар из списка.
        driver.findElements(By.xpath(".//div[@class='sc-list-item-content']//input[@value='Delete']")).get(1).click();
        driver.get(driver.getCurrentUrl());

        //Проверяем, что в корзине остались два других товара, а удаленный исчез со страницы.
        List<WebElement> newNamesInCartList = driver.findElements(By.xpath(".//div[contains(@class, 'sc-list-item-content')]//span[contains(@class, 'sc-product-title')]"));
        String[] newNamesInCartArray = {newNamesInCartList.get(0).getText(),newNamesInCartList.get(1).getText()};

        Assert.assertTrue("Quantity of merch in cart is incorrect",namesInCartList.size()-1==newNamesInCartList.size());

        Assert.assertTrue(namesInCartArray[0]+" isn't at cart", Arrays.asList(newNamesInCartArray).contains(namesInCartArray[0]));
        Assert.assertFalse(namesInCartArray[1]+" is at cart", Arrays.asList(newNamesInCartArray).contains(namesInCartArray[1]));
        Assert.assertTrue(namesInCartArray[2]+" isn't at cart", Arrays.asList(newNamesInCartArray).contains(namesInCartArray[2]));

    }

    @Test
    public void checkLogIn() {
        /*Проверяем залогин в аккаунт по появлению логина в шапке
            1. Кликаем на кнопку логина в шапке
            2. Вводим адрес и пароль
            3. Нажимаем кнопку залогина
            4. Проверяем, что в шапке появилось имя
         */
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("lambru100@yandex.ru");
        driver.findElement(By.id("ap_password")).sendKeys("gfhjkm13");
        driver.findElement(By.id("signInSubmit")).click();
        String greeting = driver.findElement(By.cssSelector("#nav-link-accountList .nav-line-1")).getText();

        Assert.assertTrue("you are not logged in", greeting.contains("lambru"));
    }

}
