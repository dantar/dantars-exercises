package figur;

public abstract class Shape {

    double x;
    double y;
    double size;

    public Shape (double x, double y, double size){
        this.x = x;
        this.y = y;
        this.size = size;
    }

    public abstract double area();

    public void move(double dX, double dY) {
        Shape.this.x +=dX;
        Shape.this.y +=dY;
    }

    public void resize(double sizeChange) {
        Shape.this.size *= sizeChange;
    }

    public double getX(){
        return Shape.this.x;
    };

    public double getY(){
        return Shape.this.y;
    };

    public double getSize(){
        return Shape.this.size;
    };

}