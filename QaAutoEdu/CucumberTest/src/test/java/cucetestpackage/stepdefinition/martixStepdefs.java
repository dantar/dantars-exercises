package cucetestpackage.stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import games.Matrix;
import org.junit.Assert;

/**
 * Created by garkusha on 16.01.2017.
 */
public class martixStepdefs {

    private Integer[][] matrixArray;
    private Matrix matrix;

    @Given("^I start program Matrix$")
    public void iStartProgramMatrix() throws Throwable {
        matrix = new Matrix();
    }

    @When("^I create matrix with size (\\d+)$")
    public void iCreateMatrixWithSize(int arg0) throws Throwable {
        matrixArray = Matrix.buildMatrix(arg0);
    }

    @Then("^Program returns Matrix$")
    public void programReturnsMatrix() throws Throwable {
        Assert.assertNotNull("Программа не вернула матрицу", matrixArray);
    }

    @Then("^Matrix have (\\d+) rows and (\\d+) columns$")
    public void matrixHaveRowsAndColumns(int arg0, int arg1) throws Throwable {
        int length = matrixArray.length;
        Assert.assertEquals("Количество строк матрицы должен быть 5, но он равен" + length,length,5);
        for(int j=0; j < matrixArray.length; j++) {
            length = matrixArray[j].length;
            Assert.assertEquals("Количество ячеек в строке "+ (j+1) +" матрицы должен быть 5, но он равен" + length,length,5);
        }
    }

    @Then("^Program returns Matrix with correct values (\\d+) in rows (\\d+) and positions (\\d+)$")
    public void programReturnsMatrixWithCorrectValuesValueInRowsRowAndPositionsColumn(int arg0, int arg1, int arg2) throws Throwable {
        Assert.assertTrue(matrixArray[arg1][arg2] == arg0);
    }
}
