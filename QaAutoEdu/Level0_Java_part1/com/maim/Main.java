package com.maim;

import com.welcome.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Hello greeting = new Hello();
        String name = greeting.setupName();
        greeting.welcome();
        System.out.println("Hello, World!");
        greeting.byebye();
    }
}
