package tests.stackoverflow.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Dantar on 19.02.2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/nixreport", "json:target/cucumber.json"},
        features = "src/test/java/tests/stackoverflow/feature",
        glue ="tests.stackoverflow.stepdefinition",
        tags = {"@Stack"}
)



/**
 * Created by Dantar on 19.02.2017.
 */
public class StackoverflowRunner {
}
