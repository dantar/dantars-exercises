package oop;

public class Square extends Shape {
    public Square(double x, double y, double size) {
        super(x, y, size);
    }

    @Override
    public double area() {
        return size*size;
    }

}
