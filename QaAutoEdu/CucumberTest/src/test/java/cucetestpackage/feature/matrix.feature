@Matrix
Feature: Matrix

  Scenario: 001 Using method buildMatrix returns Matrix
    Given I start program Matrix
    When I create matrix with size 5
    Then Program returns Matrix

  Scenario: 002 Matrix have correct number of rows
    Given I start program Matrix
    When I create matrix with size 5
    Then Matrix have 5 rows and 5 columns

  Scenario Outline: 003 Created Matrix will have certain values in cells
    Given I start program Matrix
    When I create matrix with size 5
    Then Program returns Matrix with correct values <value> in rows <row> and positions <column>
    Examples:
      | row | column | value |
      | 0   | 0      | 1    |
      | 0   | 1      | 2    |
      | 0   | 2      | 3    |
      | 0   | 3      | 4    |
      | 0   | 4      | 5    |
      | 1   | 0      | 6    |
      | 1   | 1      | 7    |
      | 1   | 2      | 8    |
      | 1   | 3      | 9    |
      | 1   | 4      | 1    |
      | 2   | 0      | 2    |
      | 2   | 1      | 3    |
      | 2   | 2      | 4    |
      | 2   | 3      | 5    |
      | 2   | 4      | 6    |
      | 3   | 0      | 7    |
      | 3   | 1      | 8    |
      | 3   | 2      | 9    |
      | 3   | 3      | 1    |
      | 3   | 4      | 2    |
      | 4   | 0      | 3    |
      | 4   | 1      | 4    |
      | 4   | 2      | 5    |
      | 4   | 3      | 6    |
      | 4   | 4      | 7    |


