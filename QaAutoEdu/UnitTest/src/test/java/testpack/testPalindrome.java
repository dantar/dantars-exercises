package testpack;

import games.Palindrome;
import org.junit.Assert;
import org.junit.Test;

public class testPalindrome {
    Palindrome palindrome = new Palindrome();

    @Test
    // Проверка определения палиндрома-слова (положительный кейс)
    public void checkPalindromeWord() {
        Assert.assertTrue("Слово 'доход' должно считаться палиндромом", palindrome.checkWord("доход"));
    }

    @Test
    // Проверка определения палиндрома-слова (отрицательный кейс)
    public void checkNotPalindromeWord() {
        Assert.assertFalse("Слово 'огонь' не должно считаться палиндромом", palindrome.checkWord("огонь"));
    }

    @Test
    // Проверка определения палиндрома-слова, без учёта регистра
    public void checkPalindromeWordReg() {
        Assert.assertTrue("Слово'Репер' должно считаться палиндромом", palindrome.checkWord("Репер"));
    }

    @Test
    // Проверка определения палиндрома-фразы (положительный кейс)
    public void checkPalindromePhrase() {
        Assert.assertTrue("Фраза 'Аргентина манит негра' должна считаться палиндромом", palindrome.checkPhrase("Аргентина манит негра"));
    }

    @Test
    // Проверка определения палиндрома-фразы (отрицательный кейс)
    public void checkNotPalindromePhrase() {
        Assert.assertFalse("Фраза 'Аргентина негра' не должна считаться палиндромом", palindrome.checkPhrase("Аргентина негра"));
    }


}



