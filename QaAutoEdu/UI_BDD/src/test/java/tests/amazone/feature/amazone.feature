@Amazone
Feature: Amazone

  Scenario: 001 When I search merch by keyword, each merchname in the results list have this word
    When I search merch by keyword "duck"
    Then I see keyword "duck" in every merchname

  Scenario: 002 When I choose a category in search results, I see less results than before
    When I search merch by keyword "duck"
    And I remember number od search results
    And I go to category BabyProducts
    Then I see less results than before

  Scenario: 003 When I add two merches in cart, I see them in cart and total price equals sum of their prices
    When I search merch by keyword "knife kitchen"
    And I choose first result with words 'eight inch' in name, remember it's name and price and add it to cart
    And I search merch by keyword "duck"
    And I choose first result, remember it's name and price and add it to cart
    And I go to cart
    Then I see names and prices of added merch in cart
    And Total price equals sum of their prices

  Scenario: 004 When I add three merches in cart, then delete second one, I see other merches are still in cart and deleted merch is not in cart
    When I search merch by keyword "Four Point Shield Blank - 16 Gauge Steel Battle Ready - Natural - One Size"
    And I choose first result, remember it's name and price and add it to cart
    And I search merch by keyword "Szco Supplies Brass Crusader Helmet"
    And I choose first result, remember it's name and price and add it to cart
    And I search merch by keyword "Sword of Tancred Crusader Sword - Medieval Blade Replica High Quality Battle Ready"
    And I choose first result, remember it's name and price and add it to cart
    And I go to cart
    And I see names and prices of added merch in cart
    And I delete second merch in cart list
    Then I see names of undeleted merch in cart, don't see name of deleted merch and amount of merch in cart is one less

  Scenario: 005 When I log in to account, I see my name in greeting
    When I go to log in page
    And I put in email and password
    And I click sign in button
    Then I see my name in greeting