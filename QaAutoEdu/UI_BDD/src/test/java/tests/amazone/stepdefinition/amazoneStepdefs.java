package tests.amazone.stepdefinition;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.PendingException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import mapping.amazone.*;


import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dantar on 19.02.2017.
 */
public class amazoneStepdefs {

    public static StartPage startPage;
    public static SearchResultsPage searchResultsPage;
    public static MerchPage merchPage;
    public static CartPage cartPage;
    public static LogInPage logInPage;
    public static Integer numberOfSearchResults;
    public String[] merchNames = new String[30];
    public String[] merchPricesText = new String[30];
    public Double[] merchPrices = new Double[30];
    public Integer numberOfAddedMerch;
    public String deletedName;
    public Integer nuberOfMerchInCart;

    public static WebDriver driver;

    @Before("@Amazone")
    public void setupStartConditions() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();

        startPage = new StartPage(driver);
        searchResultsPage = new SearchResultsPage(driver);
        merchPage = new MerchPage(driver);
        cartPage = new CartPage(driver);
        logInPage = new LogInPage(driver);

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");
        numberOfAddedMerch = 0;
    }

    @After ("@Amazone")
    public void quitDriver() {
        driver.quit();
    }

    @When("I search merch by keyword \\\"([^\\\"]*)\\\"")
    public void iSearchMerchByKeyword(String searchWord) throws Throwable {
        startPage.searchInput.sendKeys(searchWord);
        startPage.searchButton.click();
    }

    @And("^I remember number od search results$")
    public void iRememberNumberOdSearchResults() throws Throwable {
        numberOfSearchResults = searchResultsPage.getNumberOfSearchResults();
    }


    @Then("^I see keyword \"([^\"]*)\" in every merchname$")
    public void iSeeKeywordInEveryMerchname(String searchWord) throws Throwable {
        for (WebElement searchResult: searchResultsPage.searchResults){
            String searchResultText = searchResult.getText().toLowerCase();
            Assert.assertTrue("Search result " + searchResultText + " does not contain substring" + searchWord, searchResultText.contains(searchWord));
        }
    }

    @And("^I go to category BabyProducts$")
    public void iGoToCategoryBabyProducts() throws Throwable {
        searchResultsPage.babyProductsCategoryButton.click();
    }

    @Then("^I see less results than before$")
    public void iSeeLessResultsThanBefore() throws Throwable {
        Integer sortQuantity = searchResultsPage.getNumberOfSearchResults();
        Assert.assertTrue("Number of search results in target category - " + sortQuantity + " isn't less than number of total results - "+ numberOfSearchResults, numberOfSearchResults >= sortQuantity);
    }

    @And("^I choose first result with words 'eight inch' in name, remember it's name and price and add it to cart$")
    public void iChooseFirstResultWithWordsInchInNameRememberItSNameAndPriceAndAddItToCart() throws Throwable {
        searchResultsPage.eightInchKnife.click();

        merchNames[numberOfAddedMerch] = merchPage.merchName.getText();
        System.out.println(merchPage.merchName.getText());

        merchPricesText[numberOfAddedMerch] = merchPage.merchPrice.getText();
        System.out.println(merchPage.merchPrice.getText());

        merchPrices[numberOfAddedMerch] = merchPage.getMerchPrice();
        System.out.println(merchPage.getMerchPrice());

        numberOfAddedMerch ++;
        merchPage.addToCartButton.click();
    }

    @And("^I choose first result, remember it's name and price and add it to cart$")
    public void iChooseFirstResultRememberItSNameAndPriceAndAddItToCart() throws Throwable {
        searchResultsPage.searchResults.get(0).click();

        merchNames[numberOfAddedMerch] = merchPage.merchName.getText();
        System.out.println(merchPage.merchName.getText());

        merchPricesText[numberOfAddedMerch] = merchPage.merchPrice.getText();
        System.out.println(merchPage.merchPrice.getText());

        merchPrices[numberOfAddedMerch] = merchPage.getMerchPrice();
        System.out.println(merchPage.getMerchPrice());

        numberOfAddedMerch ++;
        merchPage.addToCartButton.click();
    }

    @And("^I go to cart$")
    public void iGoToCart() throws Throwable {
        merchPage.goToCartButton.click();
    }

    @Then("^I see names and prices of added merch in cart$")
    public void iSeeNamesAndPricesOfAddedMerchInCart() throws Throwable {
        //Нашли все имена в корзине
        List<WebElement> namesInCartList = cartPage.mercNamesInCart;
        String[] namesInCartArray = new String[namesInCartList.size()];
        //Записали их в аррей
        int i=0;
        for (WebElement name: namesInCartList){
            namesInCartArray[i]= name.getText();
            System.out.println(name.getText());
            i++;
        }
        //Прошли по всем именам из собранного списка и убедились, что они есть в аррее имён из корзины
        for (int j=0; j< numberOfAddedMerch; j++){
            Assert.assertTrue(j+" name is incorrect", Arrays.asList(namesInCartArray).contains(merchNames[j]));
        }


        //Нашли все цены в корзине
        List<WebElement> pricesInCartList = cartPage.mercPricesInCart;
        String[] pricesInCartArray = new String[pricesInCartList.size()];
        //Записали их в аррей
        i=0;
        for (WebElement price: pricesInCartList){
            pricesInCartArray[i]= price.getText();
            System.out.println(price.getText());
            i++;
        }
        //Прошли по всем ценам из собранного списка и убедились, что они есть в аррее цен из корзины
        for (int j=0; j < numberOfAddedMerch; j++){
            System.out.println(merchPricesText[j]);
            Assert.assertTrue(j+" price is incorrect", Arrays.asList(pricesInCartArray).contains(merchPricesText[j]));
        }

        nuberOfMerchInCart = namesInCartList.size();
    }

    @And("^Total price equals sum of their prices$")
    public void totalPriceEqualsSumOfTheirPrices() throws Throwable {
        Double totalPrice = cartPage.getTotalMerchPrice();
        Double sumOfMerchPrices = 0.0;

        for (int j=0; j< numberOfAddedMerch; j++){
            sumOfMerchPrices = sumOfMerchPrices + merchPrices[j];
            System.out.println(j+" "+sumOfMerchPrices);
        }

        Assert.assertEquals("Total price is incorrect",sumOfMerchPrices, totalPrice, 0.1);
    }

    @And("^I delete second merch in cart list$")
    public void iDeleteSecondMerchInCartList() throws Throwable {
        cartPage.deleteButtons.get(1).click();
        driver.get(driver.getCurrentUrl());
    }

    @Then("^I see names of undeleted merch in cart, don't see name of deleted merch and amount of merch in cart is one less$")
    public void iSeeNamesOfUndeletedMerchInCartDonTSeeNameOfDeletedMerchAndAmountOfMerchInCartIs() throws Throwable {
        //Нашли все имена в корзине
        List<WebElement> namesInCartList = cartPage.mercNamesInCart;
        String[] namesInCartArray = new String[namesInCartList.size()];
        //Записали их в аррей
        int i=0;
        for (WebElement name: namesInCartList){
            namesInCartArray[i]= name.getText();
            i++;
        }


        //Прошли по всем именам из собранного списка и убедились, что они есть в аррее имён из корзины
        for (int j=0; j< numberOfAddedMerch; j++){
            if (j==1){
                Assert.assertFalse(namesInCartArray[j]+" is at cart", Arrays.asList(namesInCartArray).contains(merchNames[j]));
            }
            else {
                Assert.assertTrue(j+" name is incorrect", Arrays.asList(namesInCartArray).contains(merchNames[j]));
            }
        }
        //Проверили отсутствие удалённого в списке
        Assert.assertFalse(namesInCartArray[1]+" is at cart", Arrays.asList(namesInCartArray).contains(deletedName));
        //Проверили, что количество товара в списке уменьшилось на 1
        Assert.assertTrue("Quantity of merch in cart is incorrect",namesInCartList.size()==(nuberOfMerchInCart-1));

    }

    @When("^I go to log in page$")
    public void iGoToLogInPage() throws Throwable {
        logInPage.goToLogInPageButton.click();
    }

    @And("^I put in email and password$")
    public void iPutInEmailAndPassword() throws Throwable {
        logInPage.emailInput.sendKeys("lambru100@yandex.ru");
        logInPage.passwordInput.sendKeys("gfhjkm13");
    }

    @And("^I click sign in button$")
    public void iClickSignInButton() throws Throwable {
        logInPage.signInButton.click();
    }

    @Then("^I see my name in greeting$")
    public void iSeeMyNameInGreeting() throws Throwable {
        String greeting = logInPage.greeting.getText();
        Assert.assertTrue("you are not logged in", greeting.contains("lambru"));
    }

}
