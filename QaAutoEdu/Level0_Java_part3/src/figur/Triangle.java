package figur;

public class Triangle extends Shape {
    public Triangle (double x, double y, double size) {
        super(x, y, size);
    }

    @Override
    public double area() {
        return (size*size*Math.sqrt(3)/4);
        //Для простоты, используются только равносторонние треугольники
    }

}
