@Snail
Feature: Snail

  Scenario: 002 Position of Max element in Matrix with odd size
    Given I start program Snail
    When I create snail with size 5
    Then position of maximum element is 0,4

  Scenario: 003 Position of Max element in Matrix with even size
    Given I start program Snail
    When I create snail with size 4
    Then position of maximum element is 3,0

  Scenario: 004 Snail returns Martix with particular size
    Given I start program Snail
    When I create snail with size 3
    Then program returns Matrix with size 3

  @SnailSpecial
  Scenario: 001 Max element in Snail
    Given I create snail with size 5
    Then maximum element is 25