package mapping.amazone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dantar on 18.02.2017.
 */
public class CartPage {

    private WebDriver driver;

    public CartPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//div[contains(@class, 'sc-list-item-content')]//span[contains(@class, 'sc-product-title')]")
    public List<WebElement> mercNamesInCart;

    @FindBy (xpath = ".//div[contains(@class, 'sc-list-item-content')]//span[contains(@class, 'sc-product-price')]")
    public List<WebElement> mercPricesInCart;

    @FindBy (xpath = ".//div[contains(@class, 'sc-subtotal')]//span[contains(@class,'sc-price')]")
    public WebElement totalPriceInChart;

    @FindBy (xpath = ".//div[@class='sc-list-item-content']//input[@value='Delete']")
    public List<WebElement> deleteButtons;


    public Double getTotalMerchPrice() {
        return Double.valueOf((totalPriceInChart).getText().replace("$",""));
    }

}
